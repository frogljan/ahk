﻿SetTitleMatchMode 2

WinMove, %SelfTitle%,, 1675, 179 ; placing self window just after Raid's one

ButtonClanBoss:
    Gui, CB:+owner1  ; Make the main window (Gui #1) the owner of the "about box".
    Gui +Disabled  ; Disable main window.
    Gui, CB:Add, Text, , Анкил 2 Людоеда
    Gui, CB:Add, Text, , Выберите КБ:
    Gui, CB:Add, DropDownList, w180 vCBChoice, BT|NM||UNM
    Gui, CB:Add, CheckBox, w210 h20 vCBFromStart, Начинать с бастиона
    Gui, CB:Add, CheckBox, w210 h20 vCBHideOption, Сворачивать после старта
    Gui, CB:Add, Button, Default x40 w120 h30 , START
    Gui, CB:Add, Button, x40 w120 h30 , Close
    Gui, CB:Show, w200 h210, %CBTitle%
    Return

; This section is used by the "Clan Boss" window.
CBButtonClose:
CBGuiClose:
CBGuiEscape:
    Gui, 1:-Disabled  ; Re-enable the main window (must be done prior to the next step).
    Gui Destroy  ; Destroy the about box.
    Return

CBButtonSTART:
    Gui, Submit, NoHide  ; Save the input from the user to each control's associated variable.

    FileRead, unkill_2ME, config\unkill_2ME.json
    hitSequences := JSON.Load(unkill_2ME)
    ClanBossCoords := {"UNM": 530, "NM": 410, "BT": 290}
    MS := 1000
    MSPEED := 4

    WinActivate, ahk_exe Raid.exe ; activate process Raid.exe
    CoordMode, Mouse, Relative
    Sleep, 400

    if (CBFromStart = 1) {
        FightButton(400)
        MouseMove, 1220, 400, %MSPEED% ; Select Clan Boss Mode
        Sleep, 400
        MouseClick
        Sleep, 400
    }

    Click, 1220, 600 Left, Down ; Scroll to bottom Clan Bosses
    Click, 1220, 200 Left, Up
    Sleep, 400

    SelectedCBCoords := ClanBossCoords[CBChoice]

    MouseMove, 1220, %SelectedCBCoords%, %MSPEED% ; Select Clan Boss; UNM - 530, NM, 410, BR 280
    MouseClick
    Sleep, 400

    FightButton(400)
    FightButton(10000)

    SelectedCB := hitSequences[CBChoice]
    For key, value in SelectedCB {
        turnName := value.s
        sleepDelayCalculated := value.d * MS
        %turnName%(sleepDelayCalculated)
    }

    if (CBHideOption = 1) {
        WinMinimize, %RaidTitle%
    }
    Return

FightButton(sleepDelay) {
    MouseMove, 1280, 670, %MSPEED% ; Fight Button
    MouseClick
    Sleep, %sleepDelay%
}

HitA1(sleepDelay) {
    MouseMove, 750, 300, %MSPEED% ; A1 hit
    MouseClick
    Sleep, %sleepDelay%
}

HitA2(sleepDelay) {
    MouseMove, 1200, 630, %MSPEED% ; A2 select
    MouseClick
    ; Sleep, 400
    MouseMove, 750, 300, %MSPEED% ; A2 hit
    MouseClick
    Sleep, %sleepDelay%
}

HitA3(sleepDelay) {
    MouseMove, 1340, 630, %MSPEED% ; A3 select
    MouseClick
    ; Sleep, 400
    MouseMove, 750, 300, %MSPEED% ; A3 hit
    MouseClick
    Sleep, %sleepDelay%
}

ApplyA2(sleepDelay) {
    MouseMove, 1200, 630, %MSPEED% ; A2 select
    MouseClick
    ; Sleep, 400
    MouseMove, 610, 600, %MSPEED% ; A2 apply
    MouseClick
    Sleep, %sleepDelay%
}

ApplyA3(sleepDelay) {
    MouseMove, 1340, 630, %MSPEED% ; A3 select
    MouseClick
    ; Sleep, 400
    MouseMove, 610, 600, %MSPEED% ; A3 apply
    MouseClick
    Sleep, %sleepDelay%
}

SelectAuto(sleepDelay) {
    MouseMove, 70, 650, %MSPEED% ; Select Auto mode
    MouseClick
}