﻿class SecondCounter {
    __New() {
        this.interval := 1000
        this.count := 0
        ; Tick() has an implicit parameter "this" which is a reference to
        ; the object, so we need to create a function which encapsulates
        ; "this" and the method to call:
        this.timer := ObjBindMethod(this, "Tick")
    }
    Start() {
        ; Known limitation: SetTimer requires a plain variable reference.
        timer := this.timer
        SetTimer % timer, % this.interval
        GuiControl,, StatusText, % this.count
    }
    Stop() {
        ; To turn off the timer, we must pass the same object as before:
        timer := this.timer
        SetTimer % timer, Off
        GuiControl,, StatusText, % this.count
    }
    GetTimerValue() {
        arr := [Floor(this.count / 60), Mod(this.count, 60)]
        return Format("{1:02}:{2:02}", arr*)
    }
    ; In this example, the timer calls this method: see __New()
    Tick() {
        ++this.count
        arr := [Floor(this.count / 60), Mod(this.count, 60)]
        timerString := this.GetTimerValue()
        GuiControl,, StatusText, %timerString%
    }
}
