﻿#MaxThreadsPerHotkey 2
#SingleInstance force
SetTitleMatchMode, 2

#Include <Timer>
#Include <JSON>

; Auto-execute section
; Global variables
InProgress := false
Counter := 0
RunDelay := 30
RunDelayCalculated := 60000
RunCount := 20
SelfTitle := "Рейд-Мультибой"
CBTitle := "Рейд-Анкил"
RaidTitle := "Raid: Shadow Legends"

CenterWindow(RaidTitle) ; setting window size to match image sizes
Gosub, InitGUI
SettingsLoad()
WinMove, %SelfTitle%,, 1675, 179 ; placing self window just after Raid's one
; End of auto-execute section. The script is idle until the user does something.
Return

; Button click handlers
ButtonSTART:
    Gui, Submit, NoHide  ; Save the input from the user to each control's associated variable.
    Counter := 0
    RunDelayCalculated := RunDelay * 1000
    ; MsgBox You entered "%RunCount%\n %RunDela1y%\n calculated: %RunDelayCalculated%".
    InProgress := true

    WinActivate, ahk_exe Raid.exe ; activate process Raid.exe
    CoordMode, Mouse, Relative

    ImageSearch, FoundX, FoundY, 763, 628, 967, 706, *4 images\buttons\replay.bmp
    if (ErrorLevel = 0) {
        Random, rand, 0, 100
        if (rand > 50) {
            ControlSend, , r, ahk_exe Raid.exe ; send R to raid window
        } else {
            MouseMove, %FoundX%, %FoundY%, 3
            MouseClick
        }
    } else {
        ImageSearch, FoundX, FoundY, 1200, 630, 1420, 695, *4 images\buttons\start.bmp
        if (ErrorLevel = 0) {
            MouseMove, %FoundX%, %FoundY%, 3
            MouseClick
        }
    }

    mainLoop:
    Loop {
        WinGetActiveTitle, Title ; save current window to Title
        WinActivate, ahk_exe Raid.exe ; activate process Raid.exe

        Random, rand, -10, 25
        ImageSearch, FoundX, FoundY, 763, 628, 967, 706, *4 images\buttons\replay.bmp

        if (ErrorLevel = 0) {
            FoundX += rand
            FoundY += rand
            MouseMove, %FoundX%, %FoundY%, 3
            MouseClick
            Random, FoundX, 0, 1920
            Random, FoundY, 0, 1080
            MouseMove, %FoundX%, %FoundY%, 3
        }

        WinActivate, %Title% ; activate process stored in Title var

        Counter++
        ProgressValue := Counter / RunCount * 100
        GuiControl,, MyProgress, %ProgressValue%
        GuiControl,, StatusCount, %Counter%

        timeCounter := new SecondCounter
        timeCounter.Start()

        Loop {
            WinGetActiveTitle, Title ; save current window to Title
            WinActivate, ahk_exe Raid.exe ; activate process Raid.exe
            ImageSearch, FoundX, FoundY, 763, 628, 967, 706, *4 images\buttons\replay.bmp
            if (!InProgress or (ErrorLevel = 0)) {
                WriteInfo(timeCounter.GetTimerValue() . " - replay found")
                Random, SleepDelay, 200, 800
                Sleep, %SleepDelay%
                timeCounter.Stop()
                ; check for item to sell
                CheckITEM()
                Sleep, %SleepDelay%
                Break
            } Else {
                WriteInfo(timeCounter.GetTimerValue() . " - replay not found")
                If (MoveMouse = 1) {
                    Random, FoundX, 0, 1920
                    Random, FoundY, 0, 1080
                    MouseMove, %FoundX%, %FoundY%, 3
                }
            }

            WinActivate, %Title% ; activate process stored in Title var
            If (HideOption = 1) {
                WinMinimize, %RaidTitle%
            }

            Loop, %RunDelay% {
                if (!InProgress) {
                    Break
                }
                Sleep, 1000
            }
        }

        If (Counter >= RunCount OR !InProgress) {
            MsgBox Скрипт выполнен %Counter% раз
            Break
        }

    }

    InProgress := false
    Return

ButtonAbort:
    InProgress := false
    Return

ButtonReset:
    CenterWindow(RaidTitle)
    Return

GuiClose:
GuiEscape:
ButtonClose:
    SettingsSave()
    ExitApp

InitGUI:
    ; controls
    Gui, Add, Text, x12 y12 w120 h20 , Количество запусков:
    Gui, Add, Edit, x142 y9 w70 h20 vRunCount, %RunCount%

    Gui, Add, Text, x12 y36 w120 h20 +Left, Частота проверки, сек:
    Gui, Add, Edit, x142 y33 w70 h20 vRunDelay, %RunDelay%
    Gui, Add, CheckBox, x12 y60 w210 h20 vMoveMouse, Шевелить мышкой при проверке
    Gui, Add, CheckBox, x12 y83 w210 h20 vHideOption, Сворачивать игру во время боя

    ; statistics
    Gui, Add, Text, x12 y110 w120 h20 , Прошедшее время:
    Gui, Add, Text, x142 y110 w30 h20 vStatusText, 0:00
    Gui, Add, Text, x12 y130 w120 h20 , Выполнено раз:
    Gui, Add, Text, x142 y130 w30 h20 vStatusCount, 0
    Gui, Add, Text, x12 y150 w120 h20 vStatusLog, -
    Gui, Add, Progress, x12 y170 w210 h15 cBlue vMyProgress, 0

    ; buttons
    Gui, Add, Button, Default x12 y190 w100 h30 , START
    Gui, Add, Button, x125 y190 w100 h30 , Abort
    Gui, Add, Button, x12 y230 w100 h30 , Reset
    Gui, Add, Button, x125 y230 w100 h30 , Clan Boss
    Gui, Add, Button, x125 y270 w100 h30 , Close

    Gui, Show, w240 h315, %SelfTitle%
    Return

; helper functions
SettingsLoad() {
    Global
    If (FileExist("settings.ini")) {
        IniRead, RunCount, settings.ini, settings, runCount
        IniRead, RunDelay, settings.ini, settings, runDelay
        IniRead, MoveMouse, settings.ini, settings, moveMouse
        IniRead, HideOption, settings.ini, settings, hideOption

        GuiControl,, RunCount, %RunCount%
        GuiControl,, RunDelay, %RunDelay%
        GuiControl,, MoveMouse, %MoveMouse%
        GuiControl,, HideOption, %HideOption%
    }
}

SettingsSave() {
    Global
    Gui, Submit, NoHide
    IniWrite, %RunCount%, settings.ini, settings, runCount
    IniWrite, %RunDelay%, settings.ini, settings, runDelay
    IniWrite, %MoveMouse%, settings.ini, settings, moveMouse
    IniWrite, %HideOption%, settings.ini, settings, hideOption
}

CenterWindow(WinTitle) {
    Width := 1435
    Height := 721
    ; WinSet, Style, -0x10000, %WinTitle%
    ; WinGetPos,,, Width, Height, %WinTitle%
    WinMove, %WinTitle%,, (A_ScreenWidth/2)-(Width/2), (A_ScreenHeight/2)-(Height/2), Width, Height
}

; search for item to sell
CheckITEM() {
    CoordMode, Mouse, Relative
    ; ImageSearch, FoundX, FoundY, 560, 168, 870, 283, *4 images\items\heal_shield1.bmp
    ImageSearch, FoundX, FoundY, 450, 163, 910, 283, *4 images\items\heal_boots.bmp
    if (ErrorLevel = 0) {
        MouseMove, %FoundX%, %FoundY%, 2
        MouseClick
        Sleep, 300
        MouseMove, 840, 530, 2
        MouseClick
        Sleep, 300
        MouseMove, 840, 400, 2
        MouseClick
    }
}

WriteInfo(info) {
    GuiControl,, StatusLog, %info%
    Return
}

; additional modules
#Include <ClanBoss>
